# photocrunch

photocrunch is a blog engine written in go with the express intent of serving 
http://www.amdavidson.com/

The design is not general purpose and the code may not be suitable for serving 
any other platform.

Reach out to @amdavidson on Twitter with any questions or comments. 
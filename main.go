package main

import (
	"html/template"
	"io/ioutil"
	"log"
	"math/rand"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"strconv"
	"time"
)

// PageVariables defined for use in template formatting
type PageVariables struct {
	Title    string
	ImageURL string
}

// RandomPhoto gets a random photo from a specified directory
func RandomPhoto(directory string) string {
	files, err := ioutil.ReadDir(directory)
	if err != nil {
		log.Fatal(err)
	}
	rand.Seed(time.Now().Unix())

	for {
		n := rand.Int() % len(files)
		var f, err = os.Open(filepath.Join(directory, files[n].Name()))
		if err != nil {
			log.Fatal(err)
		}

		buff := make([]byte, 512)
		if _, err = f.Read(buff); err != nil {
			log.Fatal(err)
		}
		var FileType = http.DetectContentType(buff)
		if "image" == strings.Split(FileType, "/")[0] {
			return ("/photos/" + files[n].Name())
		}
	}
}

// HomePage uses homepage.html template to render the home page
func HomePage(w http.ResponseWriter, r *http.Request) {

	HomePageVars := PageVariables{
		Title:    "www.amdavidson.com",
		ImageURL: RandomPhoto("./photos"),
	}

	t, err := template.ParseFiles("homepage.html")
	if err != nil {
		log.Print("template parsing error: ", err)
	}
	err = t.Execute(w, HomePageVars)
	if err != nil {
		log.Print("template executing error: ", err)
	}
}

// ServePhoto parses the url to either return the original file or a cached resized version
func ServePhoto (w http.ResponseWriter, r *http.Request) {
	var URLPath = strings.Split(r.URL.Path, "/")
	var PhotoName = URLPath[2]
	PhotoSize, err := strconv.ParseFloat(URLPath[3], 32)
	if err != nil {
		PhotoSize = 0.0
	}
	var AdjustedSize = math.Ceil(PhotoSize/100)*100
	log.Print("Serving ", PhotoName)
	if AdjustedSize > 0 {
		log.Print("Reducing size to ", AdjustedSize)
		http.ServeFile(w, r, ("photos/" + PhotoName))
	} else {
		http.ServeFile(w, r, ("photos/" + PhotoName))
	}
}


func main() {
	http.HandleFunc("/", HomePage)

	http.HandleFunc("/photos/", ServePhoto)

	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
